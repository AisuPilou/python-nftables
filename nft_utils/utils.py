#!/usr/bin python3

import subprocess as sp
from loguru import logger
import json



class CommandError(Exception):
    pass
class NftablesError(Exception):
    pass

def command(cmd:str):
    logger.debug("Start to execute the command : " + cmd)
    try:
        process = sp.Popen(("nft -a " + cmd).split(" "), 
                            stdout = sp.PIPE,
                            stderr = sp.PIPE,
                            encoding = "UTF-8")
        process.wait()
        out, err = process.communicate()
    except:
        logger.error("CommandError: The command could not be executed.")
        raise CommandError("The command could not be executed.")

    return out, err 

def find_rule(rule_json:dict):
    logger.
    handle = 0

    if "table" not in rule_json:
        rule_json["table"] = "filter"

    if "chain" not in rule_json:
        logger.error("NftablesError: No chain is specified in rule_json.")
        raise NftablesError("No chain is specified in rule_json.")
    
    out, err = command("list chain " + rule_json["table"] + " " + rule_json["chain"])

    out = json.load(out)
    
    i = 3 
    while i < len(out["nftables"]) and handle == 0: 
        rule = out["nftables"]["rule"]

        isRule = True
        j = 0
        while j < len(rule["expr"]) and isRule:
            match = rule["epxr"][j]
            if "match" in match:
                # Find the key
                key = ""
                if "meta" in match["match"]["left"]:
                    # generate key for interface
                    key = "interface" 
                else:
                    if match["match"]["left"]["payload"]["protocol"] in ["tcp", "udp"]:
                        if rule_json["protol4"] != match["match"]["left"]["payload"]["protocol"]:
                            isRule = False
                        # generate key for dport or sport
                        key = match["match"]["left"]["payload"]["field"]
                    else:
                        if match["match"]["left"]["payload"]["field"] == "protocol":
                            # generate key for protocol layer 4
                            key = "protol4"
                        else:
                            # generate key for ip saddr or ip daddr
                            key = match["match"]["left"]["payload"]["protocol"] + " " + match["match"]["left"]["payload"]["field"]
                # We have the key, now generate the value
                if "set" in match["match"]["right"]:
                    value = []
                    for element in match["match"]["right"]["set"]:
                        if "prefix" in element:
                            #network address
                            value.append(element["prefix"]["addr"] + "/" + str(element["prefix"]["len"]))
                        else:
                            #address IP or port
                            value.append(str(element))
                else:
                    if "prefix" in match["match"]["right"]:
                        value = match["match"]["right"]["prefix"]["addr"] + "/" + str(match["match"]["right"]["prefix"]["len"])
                    else:
                        value = str(match["match"]["right"])
                
                #check if the key and the value are same between rule_json and match
                if key not in rule_json or rule_json[key] != value:
                    isRule = False
            else:
                if match.keys()[0] != rule_json["target"]:
                    isRule = False
            j += 1

        if isRule:
            handle = rule["handle"]
        i += 1

def find_set(set_json:dict):
    out, err = command("-j list sets ")

